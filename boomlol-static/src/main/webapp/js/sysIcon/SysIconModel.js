$import('SFModel');
var SysIconModel = $createClass('SysIconModel',function(param){
	if(!param)
		param = {};
	param.name = 'sysIcon';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
