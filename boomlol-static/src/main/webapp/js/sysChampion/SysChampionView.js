$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('SysChampionModel');
var SysChampionView = $createClass('SysChampionView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'championName',name:'championName',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'cname',name:'cname',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'ename',name:'ename',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'ico',name:'ico',rules:[new CheckMaxLength({length:50})]})	
				,new SFInput({field:'coreEqp',name:'coreEqp',rules:[new CheckMaxLength({length:10})]})	
				,new SFInput({field:'note',name:'note',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'positionId',name:'positionId',rules:[new CheckMaxLength({length:10})]})	
				,new SFInput({field:'banPoint',name:'banPoint',rules:[new CheckMaxLength({length:22})]})	
				,new SFInput({field:'pickPoint',name:'pickPoint',rules:[new CheckMaxLength({length:22})]})	
				,new SFInput({field:'winPoint',name:'winPoint',rules:[new CheckMaxLength({length:22})]})	
				,new SFInput({field:'pickNum',name:'pickNum',rules:[new CheckMaxLength({length:22})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
SysChampionView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:SysChampionModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'championName',text:'championName'}
    		    		,{id:'cname',text:'cname'}
    		    		,{id:'ename',text:'ename'}
    		    		,{id:'ico',text:'ico'}
    		    		,{id:'coreEqp',text:'coreEqp'}
    		    		,{id:'note',text:'note'}
    		    		,{id:'positionId',text:'positionId'}
    		    		,{id:'banPoint',text:'banPoint'}
    		    		,{id:'pickPoint',text:'pickPoint'}
    		    		,{id:'winPoint',text:'winPoint'}
    		    		,{id:'pickNum',text:'pickNum'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
