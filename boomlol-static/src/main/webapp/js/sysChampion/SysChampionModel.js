$import('SFModel');
var SysChampionModel = $createClass('SysChampionModel',function(param){
	if(!param)
		param = {};
	param.name = 'sysChampion';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
