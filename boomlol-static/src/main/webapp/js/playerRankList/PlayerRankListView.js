$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('PlayerRankListModel');
var PlayerRankListView = $createClass('PlayerRankListView',function(){
		this.pojoControls = [
		new SFInput({field:'playerId',name:'playerId',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'mostUseChampionId',name:'mostUseChampionId',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
					
	];
	this.pojoHidden=["id"];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
PlayerRankListView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:PlayerRankListModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'playerId',text:'playerId'}
    		    		,{id:'mostUseChampionId',text:'mostUseChampionId'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
