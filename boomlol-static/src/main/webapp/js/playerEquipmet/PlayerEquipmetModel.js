$import('SFModel');
var PlayerEquipmetModel = $createClass('PlayerEquipmetModel',function(param){
	if(!param)
		param = {};
	param.name = 'playerEquipmet';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
