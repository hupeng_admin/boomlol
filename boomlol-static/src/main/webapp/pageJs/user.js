$import('UserView');
$import('UserModel');
$import('SFPage');
$ready(function(){
	var page= new SFPage({
		menuList:menuList,
		view : new UserView(),
		model: UserModel.get(),
		viewMsgs:['add','del','update']
	})
});
	
