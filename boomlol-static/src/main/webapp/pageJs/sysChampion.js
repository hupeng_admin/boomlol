$import('SysChampionView');
$import('SysChampionModel');
$import('SFPage');
$ready(function(){
	var page= new SFPage({
		menuList:menuList,
		view : new SysChampionView(),
		model: SysChampionModel.get(),
		viewMsgs:['add','del','update']
	})
});
	
