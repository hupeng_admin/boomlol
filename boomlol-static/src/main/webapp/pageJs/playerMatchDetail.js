$import('PlayerMatchDetailView');
$import('PlayerMatchDetailModel');
$import('SFPage');
$ready(function(){
	var page= new SFPage({
		menuList:menuList,
		view : new PlayerMatchDetailView(),
		model: PlayerMatchDetailModel.get(),
		viewMsgs:['add','del','update']
	})
});
	
