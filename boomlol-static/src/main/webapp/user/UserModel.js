$import('SFModel');
var UserModel = $createClass('UserModel',function(param){
	if(!param)
		param = {};
	param.name = 'user';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
