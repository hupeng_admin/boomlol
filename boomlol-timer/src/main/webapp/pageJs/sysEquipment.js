$import('SysEquipmentView');
$import('SysEquipmentModel');
$import('SFPage');
$ready(function(){
	var page= new SFPage({
		menuList:menuList,
		view : new SysEquipmentView(),
		model: SysEquipmentModel.get(),
		viewMsgs:['add','del','update']
	})
});
	
