$import('SFModel');
var PlayerMostUseChampionModel = $createClass('PlayerMostUseChampionModel',function(param){
	if(!param)
		param = {};
	param.name = 'playerMostUseChampion';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
