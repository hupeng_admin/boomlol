$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('PlayerMatchDetailModel');
var PlayerMatchDetailView = $createClass('PlayerMatchDetailView',function(){
		this.pojoControls = [
		new SFInput({field:'matchId',name:'比赛Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'playerId',name:'玩家Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'championId',name:'英雄Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'numKill',name:'杀人数',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'numAssists',name:'助功数',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'numDeath',name:'死亡数',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'goldEarned',name:'金钱数',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'totalDamage',name:'总伤害',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'physicalDamage',name:'物理伤害',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'magicDamage',name:'魔法伤害',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'largestMultiKill',name:'最高多杀',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'largestKillingSpree',name:'最高连杀',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'exp',name:'经验值',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'demagePoint',name:'伤害占比',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'minionsKilled',name:'补兵数',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'minionsKilledPerMin',name:'补兵/分钟',rules:[new CheckEmpty,new CheckMaxLength({length:22})]})	
				,new SFInput({field:'goldEarnedPerMin',name:'金钱/分钟',rules:[new CheckEmpty,new CheckMaxLength({length:22})]})	
				,new SFInput({field:'skill1',name:'技能一',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'skill2',name:'技能二',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
					
	];
	this.pojoHidden=["id"];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
PlayerMatchDetailView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:PlayerMatchDetailModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'matchId',text:'比赛Id'}
    		    		,{id:'playerId',text:'玩家Id'}
    		    		,{id:'championId',text:'英雄Id'}
    		    		,{id:'numKill',text:'杀人数'}
    		    		,{id:'numAssists',text:'助功数'}
    		    		,{id:'numDeath',text:'死亡数'}
    		    		,{id:'goldEarned',text:'金钱数'}
    		    		,{id:'totalDamage',text:'总伤害'}
    		    		,{id:'physicalDamage',text:'物理伤害'}
    		    		,{id:'magicDamage',text:'魔法伤害'}
    		    		,{id:'largestMultiKill',text:'最高多杀'}
    		    		,{id:'largestKillingSpree',text:'最高连杀'}
    		    		,{id:'exp',text:'经验值'}
    		    		,{id:'demagePoint',text:'伤害占比'}
    		    		,{id:'minionsKilled',text:'补兵数'}
    		    		,{id:'minionsKilledPerMin',text:'补兵/分钟'}
    		    		,{id:'goldEarnedPerMin',text:'金钱/分钟'}
    		    		,{id:'skill1',text:'技能一'}
    		    		,{id:'skill2',text:'技能二'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
