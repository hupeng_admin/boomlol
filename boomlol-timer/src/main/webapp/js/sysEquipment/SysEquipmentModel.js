$import('SFModel');
var SysEquipmentModel = $createClass('SysEquipmentModel',function(param){
	if(!param)
		param = {};
	param.name = 'sysEquipment';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
