$import('SFModel');
var PlayerRankListModel = $createClass('PlayerRankListModel',function(param){
	if(!param)
		param = {};
	param.name = 'playerRankList';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
