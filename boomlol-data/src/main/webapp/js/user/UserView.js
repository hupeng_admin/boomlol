$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('UserModel');
var UserView = $createClass('UserView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'uname',name:'uname',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'playerId',name:'playerId',rules:[new CheckMaxLength({length:19})]})	
				,new SFInput({field:'status',name:'status',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'email',name:'email',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'phone',name:'phone',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'sex',name:'sex',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'birthday',name:'birthday',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
UserView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:UserModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'uname',text:'uname'}
    		    		,{id:'playerId',text:'playerId'}
    		    		,{id:'status',text:'status'}
    		    		,{id:'email',text:'email'}
    		    		,{id:'phone',text:'phone'}
    		    		,{id:'sex',text:'sex'}
    		    		,{id:'birthday',text:'birthday'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
