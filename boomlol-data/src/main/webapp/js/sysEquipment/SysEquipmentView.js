$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('SysEquipmentModel');
var SysEquipmentView = $createClass('SysEquipmentView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'eqpName',name:'eqpName',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'eqpIco',name:'eqpIco',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
SysEquipmentView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:SysEquipmentModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'eqpName',text:'eqpName'}
    		    		,{id:'eqpIco',text:'eqpIco'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
