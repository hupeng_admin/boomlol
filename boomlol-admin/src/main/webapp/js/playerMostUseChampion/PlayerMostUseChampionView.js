$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('PlayerMostUseChampionModel');
var PlayerMostUseChampionView = $createClass('PlayerMostUseChampionView',function(){
		this.pojoControls = [
		new SFInput({field:'championId',name:'英雄Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'playerId',name:'玩家Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
					
	];
	this.pojoHidden=["id"];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
PlayerMostUseChampionView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:PlayerMostUseChampionModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'championId',text:'英雄Id'}
    		    		,{id:'playerId',text:'玩家Id'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
