$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('PlayerMatchModel');
var PlayerMatchView = $createClass('PlayerMatchView',function(){
		this.pojoControls = [
		new SFInput({field:'championId',name:'英雄Id',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'matchType',name:'比赛类型',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'win',name:'胜负',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'playerId',name:'玩家Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'beginTimeByString',name:'开始时间',rules:[new CheckEmpty]})	
				,new SFInput({field:'endTimeByString',name:'结束时间',rules:[new CheckEmpty]})	
				,new SFInput({field:'delayTime',name:'持续时间',rules:[new CheckMaxLength({length:19})]})	
					
	];
	this.pojoHidden=["id"];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
PlayerMatchView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:PlayerMatchModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'championId',text:'英雄Id'}
    		    		,{id:'matchType',text:'比赛类型'}
    		    		,{id:'win',text:'胜负'}
    		    		,{id:'playerId',text:'玩家Id'}
    		    		,{id:'beginTime',text:'开始时间'}
    		    		,{id:'endTime',text:'结束时间'}
    		    		,{id:'delayTime',text:'持续时间'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
