$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('SysSkillModel');
var SysSkillView = $createClass('SysSkillView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:10})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'skillName',name:'技能名',rules:[new CheckMaxLength({length:50})]})	
				,new SFInput({field:'skillIco',name:'技能图标',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'skillPath',name:'图标路径',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
SysSkillView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:SysSkillModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'skillName',text:'技能名'}
    		    		,{id:'skillIco',text:'技能图标'}
    		    		,{id:'skillPath',text:'图标路径'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
