$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('SysIconModel');
var SysIconView = $createClass('SysIconView',function(){
		this.pojoControls = [
		new SFInput({field:'path',name:'路径',rules:[new CheckEmpty,new CheckMaxLength({length:200})]})	
					
	];
	this.pojoHidden=["id"];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
SysIconView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:SysIconModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'path',text:'路径'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
