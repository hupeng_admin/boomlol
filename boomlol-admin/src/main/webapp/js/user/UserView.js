$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('UserModel');
var UserView = $createClass('UserView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'uname',name:'用户名',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'playerId',name:'玩家Id',rules:[new CheckMaxLength({length:19})]})	
				,new SFInput({field:'status',name:'状态',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'email',name:'email',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'phone',name:'电话',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'sex',name:'性别',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFDate({field:'birthday',name:'生日',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
UserView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:UserModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'uname',text:'用户名'}
    		    		,{id:'playerId',text:'玩家Id'}
    		    		,{id:'status',text:'状态'}
    		    		,{id:'email',text:'email'}
    		    		,{id:'phone',text:'电话'}
    		    		,{id:'sex',text:'性别'}
    		    		,{id:'birthday',text:'生日'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
