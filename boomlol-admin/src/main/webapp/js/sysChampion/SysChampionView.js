$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('SysChampionModel');
var SysChampionView = $createClass('SysChampionView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'championName',name:'英雄名',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'cname',name:'中文名',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'ename',name:'英文名',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'ico',name:'英雄图像',rules:[new CheckMaxLength({length:50})]})	
				,new SFInput({field:'coreEqp',name:'核心装',rules:[new CheckMaxLength({length:10})]})	
				,new SFInput({field:'note',name:'攻略',rules:[new CheckEmpty,new CheckMaxLength({length:50})]})	
				,new SFInput({field:'positionId',name:'位置Id',rules:[new CheckMaxLength({length:10})]})	
				,new SFInput({field:'banPoint',name:'被禁率',rules:[new CheckMaxLength({length:22})]})	
				,new SFInput({field:'pickPoint',name:'出场率',rules:[new CheckMaxLength({length:22})]})	
				,new SFInput({field:'winPoint',name:'胜率',rules:[new CheckMaxLength({length:22})]})	
				,new SFInput({field:'pickNum',name:'出场次数',rules:[new CheckMaxLength({length:22})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
SysChampionView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:SysChampionModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'championName',text:'英雄名'}
    		    		,{id:'cname',text:'中文名'}
    		    		,{id:'ename',text:'英文名'}
    		    		,{id:'ico',text:'英雄图像'}
    		    		,{id:'coreEqp',text:'核心装'}
    		    		,{id:'note',text:'攻略'}
    		    		,{id:'positionId',text:'位置Id'}
    		    		,{id:'banPoint',text:'被禁率'}
    		    		,{id:'pickPoint',text:'出场率'}
    		    		,{id:'winPoint',text:'胜率'}
    		    		,{id:'pickNum',text:'出场次数'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
