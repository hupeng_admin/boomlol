import java.util.List;

import com.fastwebx.db.query.Query;
import com.fastwebx.db.test.TestBase;
import com.lol.biz.player.dao.PlayerDao;
import com.lol.biz.player.domain.Player;



public class TestPlayer extends TestBase<PlayerDao>{
	public void testSelect(){
		List<Player> list = this.getServer().findAll();
		for (Player player : list) {
			System.out.println(player.getPlayerName());
		}
	}
	
	public void testUpd(){
		Player player = this.getServer().findOne("id",3);
		player.setPlayerName("小莫");
		this.getServer().update(player);
	}
	
	public void TestDel(){
		Player player = this.getServer().findOne("id",12);
		this.getServer().del(player);
	}
	
	public void jiaojianSelect(){
		Query query = new Query();
		query.sql("id = 3");
		List<Player> list = this.getServer().find(query);
		for (Player player : list) {
			System.out.println(player.getTierId());
		}
	}
}
