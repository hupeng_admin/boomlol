$import('PlayerView');
$import('PlayerModel');
$import('SFPage');
$ready(function(){
	var page= new SFPage({
		menuList:menuList,
		view : new PlayerView(),
		model: PlayerModel.get(),
		viewMsgs:['add','del','update']
	})
});
	
