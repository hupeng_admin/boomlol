$import('SFModel');
var PlayerModel = $createClass('PlayerModel',function(param){
	if(!param)
		param = {};
	param.name = 'player';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
