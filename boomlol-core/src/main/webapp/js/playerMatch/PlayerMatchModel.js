$import('SFModel');
var PlayerMatchModel = $createClass('PlayerMatchModel',function(param){
	if(!param)
		param = {};
	param.name = 'playerMatch';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
