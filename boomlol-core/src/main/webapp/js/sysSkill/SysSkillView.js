$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('SysSkillModel');
var SysSkillView = $createClass('SysSkillView',function(){
		this.pk = new SFInput({field:'id',name:'id',rules:[new CheckEmpty,new CheckMaxLength({length:10})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'skillName',name:'skillName',rules:[new CheckMaxLength({length:50})]})	
				,new SFInput({field:'skillIco',name:'skillIco',rules:[new CheckEmpty,new CheckMaxLength({length:10})]})	
				,new SFInput({field:'skillPath',name:'skillPath',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
SysSkillView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:SysSkillModel.get(),
		col:[
			    		{id:'id',text:'id'}
    		    		,{id:'skillName',text:'skillName'}
    		    		,{id:'skillIco',text:'skillIco'}
    		    		,{id:'skillPath',text:'skillPath'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
