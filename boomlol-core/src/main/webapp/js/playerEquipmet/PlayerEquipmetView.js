$import('ValidateRule');
$import('SFGridView');
$import('SFGrid');
$import('SFInput');
$import('SFDate');
$import('PlayerEquipmetModel');
var PlayerEquipmetView = $createClass('PlayerEquipmetView',function(){
		this.pk = new SFInput({field:'equipmentId',name:'装备Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]});
		this.pojoControls = [
    	this.pk
		,new SFInput({field:'matchDetailId',name:'比赛详情Id',rules:[new CheckEmpty,new CheckMaxLength({length:19})]})	
				,new SFInput({field:'type',name:'类型',rules:[new CheckMaxLength({length:10})]})	
					
	];
	this.schControl =  [

	];
	this.leftBtns = [
		this.createAddBtn()
	];
	this.SFGridView();
},'SFGridView');
PlayerEquipmetView.prototype.buildGrid = function(){
	var grid = new SFGrid({
		model:PlayerEquipmetModel.get(),
		col:[
			    		{id:'matchDetailId',text:'比赛详情Id'}
    		    		,{id:'type',text:'类型'}
    		    		,{id:'equipmentId',text:'装备Id'}
    					
		],
			
		linebutton:[
			this.createUpdateBtn(),
			this.createDelBtn()
		]
	});
	return grid;
}
