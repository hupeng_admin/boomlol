package com.lol.web.playerMatch;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerMatch.domain.PlayerMatch;
import com.lol.biz.playerMatch.dao.PlayerMatchDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updatePlayerMatch.do")
public class UpdatePlayerMatch extends UpdateCtrl<PlayerMatch>{
	public UpdatePlayerMatch(){
		
	}
	@Resource
	private PlayerMatchDao playerMatchDao; 
	
	@Override
	public ICrud<PlayerMatch> acqDao() {
		return playerMatchDao;
	}
}
