package com.lol.web.playerMatch;

import com.lol.biz.playerMatch.domain.PlayerMatch;
import com.lol.biz.playerMatch.dao.PlayerMatchDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addPlayerMatch")
public class AddPlayerMatch extends AddCtrl<PlayerMatch>{
	public AddPlayerMatch(){
		
	}

	@Resource
	private PlayerMatchDao playerMatchDao; 
	
	@Override
	public ICrud<PlayerMatch> acqDao() {
		return playerMatchDao;
	}
}
