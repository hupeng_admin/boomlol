package com.lol.web.playerMatch;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerMatch.domain.PlayerMatch;
import com.lol.biz.playerMatch.dao.PlayerMatchDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delPlayerMatch.do")
public class DelPlayerMatch extends DelCtrl<PlayerMatch>{
	public DelPlayerMatch(){
		
	}
	@Resource
	private PlayerMatchDao playerMatchDao; 
	
	@Override
	public ICrud<PlayerMatch> acqDao() {
		return playerMatchDao;
	}
}
