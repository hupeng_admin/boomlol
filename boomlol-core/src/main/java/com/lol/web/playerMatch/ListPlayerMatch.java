package com.lol.web.playerMatch;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.lol.biz.playerMatch.dao.PlayerMatchDao;
import com.lol.biz.playerMatch.domain.PlayerMatch;
@Controller
@RequestMapping("/listPlayerMatch")
public class ListPlayerMatch extends SchCtrl<PlayerMatch,SchParam>{
	
	public ListPlayerMatch(){
		this.needFormatDate = true;
	}
	@Resource
	private PlayerMatchDao playerMatchDao; 
	
	@Override
	public IScher<PlayerMatch> acqDao() {
		return playerMatchDao;
	}
}
