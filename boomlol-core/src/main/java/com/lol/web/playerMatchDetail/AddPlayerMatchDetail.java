package com.lol.web.playerMatchDetail;

import com.lol.biz.playerMatchDetail.domain.PlayerMatchDetail;
import com.lol.biz.playerMatchDetail.dao.PlayerMatchDetailDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addPlayerMatchDetail")
public class AddPlayerMatchDetail extends AddCtrl<PlayerMatchDetail>{
	public AddPlayerMatchDetail(){
		
	}

	@Resource
	private PlayerMatchDetailDao playerMatchDetailDao; 
	
	@Override
	public ICrud<PlayerMatchDetail> acqDao() {
		return playerMatchDetailDao;
	}
}
