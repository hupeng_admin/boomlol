package com.lol.web.playerMatchDetail;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerMatchDetail.domain.PlayerMatchDetail;
import com.lol.biz.playerMatchDetail.dao.PlayerMatchDetailDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updatePlayerMatchDetail.do")
public class UpdatePlayerMatchDetail extends UpdateCtrl<PlayerMatchDetail>{
	public UpdatePlayerMatchDetail(){
		
	}
	@Resource
	private PlayerMatchDetailDao playerMatchDetailDao; 
	
	@Override
	public ICrud<PlayerMatchDetail> acqDao() {
		return playerMatchDetailDao;
	}
}
