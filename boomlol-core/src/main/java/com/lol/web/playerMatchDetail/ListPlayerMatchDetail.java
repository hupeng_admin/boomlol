package com.lol.web.playerMatchDetail;

import com.lol.biz.playerMatchDetail.domain.PlayerMatchDetail;
import com.lol.biz.playerMatchDetail.dao.PlayerMatchDetailDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listPlayerMatchDetail")
public class ListPlayerMatchDetail extends SchCtrl<PlayerMatchDetail,SchParam>{
	
	public ListPlayerMatchDetail(){
		this.needFormatDate = true;
	}
	@Resource
	private PlayerMatchDetailDao playerMatchDetailDao; 
	
	@Override
	public IScher<PlayerMatchDetail> acqDao() {
		return playerMatchDetailDao;
	}
}
