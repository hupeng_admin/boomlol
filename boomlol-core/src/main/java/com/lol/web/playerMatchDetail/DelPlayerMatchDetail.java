package com.lol.web.playerMatchDetail;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerMatchDetail.domain.PlayerMatchDetail;
import com.lol.biz.playerMatchDetail.dao.PlayerMatchDetailDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delPlayerMatchDetail.do")
public class DelPlayerMatchDetail extends DelCtrl<PlayerMatchDetail>{
	public DelPlayerMatchDetail(){
		
	}
	@Resource
	private PlayerMatchDetailDao playerMatchDetailDao; 
	
	@Override
	public ICrud<PlayerMatchDetail> acqDao() {
		return playerMatchDetailDao;
	}
}
