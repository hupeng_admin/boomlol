package com.lol.web.playerMostUseChampion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerMostUseChampion.domain.PlayerMostUseChampion;
import com.lol.biz.playerMostUseChampion.dao.PlayerMostUseChampionDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delPlayerMostUseChampion.do")
public class DelPlayerMostUseChampion extends DelCtrl<PlayerMostUseChampion>{
	public DelPlayerMostUseChampion(){
		
	}
	@Resource
	private PlayerMostUseChampionDao playerMostUseChampionDao; 
	
	@Override
	public ICrud<PlayerMostUseChampion> acqDao() {
		return playerMostUseChampionDao;
	}
}
