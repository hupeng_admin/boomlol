package com.lol.web.playerMostUseChampion;

import com.lol.biz.playerMostUseChampion.domain.PlayerMostUseChampion;
import com.lol.biz.playerMostUseChampion.dao.PlayerMostUseChampionDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listPlayerMostUseChampion")
public class ListPlayerMostUseChampion extends SchCtrl<PlayerMostUseChampion,SchParam>{
	
	public ListPlayerMostUseChampion(){
		this.needFormatDate = true;
	}
	@Resource
	private PlayerMostUseChampionDao playerMostUseChampionDao; 
	
	@Override
	public IScher<PlayerMostUseChampion> acqDao() {
		return playerMostUseChampionDao;
	}
}
