package com.lol.web.playerMostUseChampion;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerMostUseChampion.domain.PlayerMostUseChampion;
import com.lol.biz.playerMostUseChampion.dao.PlayerMostUseChampionDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updatePlayerMostUseChampion.do")
public class UpdatePlayerMostUseChampion extends UpdateCtrl<PlayerMostUseChampion>{
	public UpdatePlayerMostUseChampion(){
		
	}
	@Resource
	private PlayerMostUseChampionDao playerMostUseChampionDao; 
	
	@Override
	public ICrud<PlayerMostUseChampion> acqDao() {
		return playerMostUseChampionDao;
	}
}
