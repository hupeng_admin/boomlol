package com.lol.web.playerMostUseChampion;

import com.lol.biz.playerMostUseChampion.domain.PlayerMostUseChampion;
import com.lol.biz.playerMostUseChampion.dao.PlayerMostUseChampionDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addPlayerMostUseChampion")
public class AddPlayerMostUseChampion extends AddCtrl<PlayerMostUseChampion>{
	public AddPlayerMostUseChampion(){
		
	}

	@Resource
	private PlayerMostUseChampionDao playerMostUseChampionDao; 
	
	@Override
	public ICrud<PlayerMostUseChampion> acqDao() {
		return playerMostUseChampionDao;
	}
}
