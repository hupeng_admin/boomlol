package com.lol.web.sysEquipment;

import com.lol.biz.sysEquipment.domain.SysEquipment;
import com.lol.biz.sysEquipment.dao.SysEquipmentDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listSysEquipment")
public class ListSysEquipment extends SchCtrl<SysEquipment,SchParam>{
	
	public ListSysEquipment(){
		this.needFormatDate = true;
	}
	@Resource
	private SysEquipmentDao sysEquipmentDao; 
	
	@Override
	public IScher<SysEquipment> acqDao() {
		return sysEquipmentDao;
	}
}
