package com.lol.web.sysEquipment;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysEquipment.domain.SysEquipment;
import com.lol.biz.sysEquipment.dao.SysEquipmentDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delSysEquipment.do")
public class DelSysEquipment extends DelCtrl<SysEquipment>{
	public DelSysEquipment(){
		
	}
	@Resource
	private SysEquipmentDao sysEquipmentDao; 
	
	@Override
	public ICrud<SysEquipment> acqDao() {
		return sysEquipmentDao;
	}
}
