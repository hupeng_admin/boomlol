package com.lol.web.sysEquipment;

import com.lol.biz.sysEquipment.domain.SysEquipment;
import com.lol.biz.sysEquipment.dao.SysEquipmentDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addSysEquipment")
public class AddSysEquipment extends AddCtrl<SysEquipment>{
	public AddSysEquipment(){
		
	}

	@Resource
	private SysEquipmentDao sysEquipmentDao; 
	
	@Override
	public ICrud<SysEquipment> acqDao() {
		return sysEquipmentDao;
	}
}
