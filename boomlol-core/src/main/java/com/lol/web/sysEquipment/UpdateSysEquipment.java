package com.lol.web.sysEquipment;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysEquipment.domain.SysEquipment;
import com.lol.biz.sysEquipment.dao.SysEquipmentDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updateSysEquipment.do")
public class UpdateSysEquipment extends UpdateCtrl<SysEquipment>{
	public UpdateSysEquipment(){
		
	}
	@Resource
	private SysEquipmentDao sysEquipmentDao; 
	
	@Override
	public ICrud<SysEquipment> acqDao() {
		return sysEquipmentDao;
	}
}
