package com.lol.web.playerEquipmet;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerEquipmet.domain.PlayerEquipmet;
import com.lol.biz.playerEquipmet.dao.PlayerEquipmetDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delPlayerEquipmet.do")
public class DelPlayerEquipmet extends DelCtrl<PlayerEquipmet>{
	public DelPlayerEquipmet(){
		
	}
	@Resource
	private PlayerEquipmetDao playerEquipmetDao; 
	
	@Override
	public ICrud<PlayerEquipmet> acqDao() {
		return playerEquipmetDao;
	}
}
