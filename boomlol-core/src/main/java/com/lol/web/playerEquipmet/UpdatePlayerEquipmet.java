package com.lol.web.playerEquipmet;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerEquipmet.domain.PlayerEquipmet;
import com.lol.biz.playerEquipmet.dao.PlayerEquipmetDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updatePlayerEquipmet.do")
public class UpdatePlayerEquipmet extends UpdateCtrl<PlayerEquipmet>{
	public UpdatePlayerEquipmet(){
		
	}
	@Resource
	private PlayerEquipmetDao playerEquipmetDao; 
	
	@Override
	public ICrud<PlayerEquipmet> acqDao() {
		return playerEquipmetDao;
	}
}
