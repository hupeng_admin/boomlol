package com.lol.web.playerEquipmet;

import com.lol.biz.playerEquipmet.domain.PlayerEquipmet;
import com.lol.biz.playerEquipmet.dao.PlayerEquipmetDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addPlayerEquipmet")
public class AddPlayerEquipmet extends AddCtrl<PlayerEquipmet>{
	public AddPlayerEquipmet(){
		
	}

	@Resource
	private PlayerEquipmetDao playerEquipmetDao; 
	
	@Override
	public ICrud<PlayerEquipmet> acqDao() {
		return playerEquipmetDao;
	}
}
