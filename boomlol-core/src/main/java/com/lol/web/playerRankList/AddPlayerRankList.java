package com.lol.web.playerRankList;

import com.lol.biz.playerRankList.domain.PlayerRankList;
import com.lol.biz.playerRankList.dao.PlayerRankListDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addPlayerRankList")
public class AddPlayerRankList extends AddCtrl<PlayerRankList>{
	public AddPlayerRankList(){
		
	}

	@Resource
	private PlayerRankListDao playerRankListDao; 
	
	@Override
	public ICrud<PlayerRankList> acqDao() {
		return playerRankListDao;
	}
}
