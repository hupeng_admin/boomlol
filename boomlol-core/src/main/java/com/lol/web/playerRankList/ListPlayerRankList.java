package com.lol.web.playerRankList;

import com.lol.biz.playerRankList.domain.PlayerRankList;
import com.lol.biz.playerRankList.dao.PlayerRankListDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listPlayerRankList")
public class ListPlayerRankList extends SchCtrl<PlayerRankList,SchParam>{
	
	public ListPlayerRankList(){
		this.needFormatDate = true;
	}
	@Resource
	private PlayerRankListDao playerRankListDao; 
	
	@Override
	public IScher<PlayerRankList> acqDao() {
		return playerRankListDao;
	}
}
