package com.lol.web.playerRankList;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerRankList.domain.PlayerRankList;
import com.lol.biz.playerRankList.dao.PlayerRankListDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updatePlayerRankList.do")
public class UpdatePlayerRankList extends UpdateCtrl<PlayerRankList>{
	public UpdatePlayerRankList(){
		
	}
	@Resource
	private PlayerRankListDao playerRankListDao; 
	
	@Override
	public ICrud<PlayerRankList> acqDao() {
		return playerRankListDao;
	}
}
