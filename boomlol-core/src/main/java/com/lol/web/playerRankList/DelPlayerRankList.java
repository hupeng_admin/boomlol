package com.lol.web.playerRankList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.playerRankList.domain.PlayerRankList;
import com.lol.biz.playerRankList.dao.PlayerRankListDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delPlayerRankList.do")
public class DelPlayerRankList extends DelCtrl<PlayerRankList>{
	public DelPlayerRankList(){
		
	}
	@Resource
	private PlayerRankListDao playerRankListDao; 
	
	@Override
	public ICrud<PlayerRankList> acqDao() {
		return playerRankListDao;
	}
}
