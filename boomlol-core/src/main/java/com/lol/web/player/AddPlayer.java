package com.lol.web.player;

import com.lol.biz.player.domain.Player;
import com.lol.biz.player.dao.PlayerDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addPlayer")
public class AddPlayer extends AddCtrl<Player>{
	public AddPlayer(){
		
	}

	@Resource
	private PlayerDao playerDao; 
	
	@Override
	public ICrud<Player> acqDao() {
		return playerDao;
	}
}
