package com.lol.web.player;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.lol.biz.player.dao.PlayerDao;
import com.lol.biz.player.domain.Player;
@Controller
@RequestMapping("/listPlayer")
public class ListPlayer extends SchCtrl<Player,SchParam>{
	
	public ListPlayer(){
		this.needFormatDate = true;
	}
	@Resource
	private PlayerDao playerDao; 
	
	@Override
	public IScher<Player> acqDao() {
		return playerDao;
	}
}
