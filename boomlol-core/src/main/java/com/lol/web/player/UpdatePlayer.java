package com.lol.web.player;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.player.domain.Player;
import com.lol.biz.player.dao.PlayerDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updatePlayer.do")
public class UpdatePlayer extends UpdateCtrl<Player>{
	public UpdatePlayer(){
		
	}
	@Resource
	private PlayerDao playerDao; 
	
	@Override
	public ICrud<Player> acqDao() {
		return playerDao;
	}
}
