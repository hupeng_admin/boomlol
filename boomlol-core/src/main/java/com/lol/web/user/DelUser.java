package com.lol.web.user;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.user.domain.User;
import com.lol.biz.user.dao.UserDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delUser.do")
public class DelUser extends DelCtrl<User>{
	public DelUser(){
		
	}
	@Resource
	private UserDao userDao; 
	
	@Override
	public ICrud<User> acqDao() {
		return userDao;
	}
}
