package com.lol.web.user;

import com.lol.biz.user.domain.User;
import com.lol.biz.user.dao.UserDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addUser")
public class AddUser extends AddCtrl<User>{
	public AddUser(){
		
	}

	@Resource
	private UserDao userDao; 
	
	@Override
	public ICrud<User> acqDao() {
		return userDao;
	}
}
