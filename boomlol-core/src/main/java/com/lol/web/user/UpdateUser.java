package com.lol.web.user;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.user.domain.User;
import com.lol.biz.user.dao.UserDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updateUser.do")
public class UpdateUser extends UpdateCtrl<User>{
	public UpdateUser(){
		
	}
	@Resource
	private UserDao userDao; 
	
	@Override
	public ICrud<User> acqDao() {
		return userDao;
	}
}
