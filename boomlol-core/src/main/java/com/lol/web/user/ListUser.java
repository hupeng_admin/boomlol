package com.lol.web.user;

import com.lol.biz.user.domain.User;
import com.lol.biz.user.dao.UserDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listUser")
public class ListUser extends SchCtrl<User,SchParam>{
	
	public ListUser(){
		this.needFormatDate = true;
	}
	@Resource
	private UserDao userDao; 
	
	@Override
	public IScher<User> acqDao() {
		return userDao;
	}
}
