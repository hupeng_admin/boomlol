package com.lol.web.sysSkill;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysSkill.domain.SysSkill;
import com.lol.biz.sysSkill.dao.SysSkillDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delSysSkill.do")
public class DelSysSkill extends DelCtrl<SysSkill>{
	public DelSysSkill(){
		
	}
	@Resource
	private SysSkillDao sysSkillDao; 
	
	@Override
	public ICrud<SysSkill> acqDao() {
		return sysSkillDao;
	}
}
