package com.lol.web.sysSkill;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysSkill.domain.SysSkill;
import com.lol.biz.sysSkill.dao.SysSkillDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updateSysSkill.do")
public class UpdateSysSkill extends UpdateCtrl<SysSkill>{
	public UpdateSysSkill(){
		
	}
	@Resource
	private SysSkillDao sysSkillDao; 
	
	@Override
	public ICrud<SysSkill> acqDao() {
		return sysSkillDao;
	}
}
