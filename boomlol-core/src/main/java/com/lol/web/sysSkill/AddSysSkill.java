package com.lol.web.sysSkill;

import com.lol.biz.sysSkill.domain.SysSkill;
import com.lol.biz.sysSkill.dao.SysSkillDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addSysSkill")
public class AddSysSkill extends AddCtrl<SysSkill>{
	public AddSysSkill(){
		
	}

	@Resource
	private SysSkillDao sysSkillDao; 
	
	@Override
	public ICrud<SysSkill> acqDao() {
		return sysSkillDao;
	}
}
