package com.lol.web.sysSkill;

import com.lol.biz.sysSkill.domain.SysSkill;
import com.lol.biz.sysSkill.dao.SysSkillDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listSysSkill")
public class ListSysSkill extends SchCtrl<SysSkill,SchParam>{
	
	public ListSysSkill(){
		this.needFormatDate = true;
	}
	@Resource
	private SysSkillDao sysSkillDao; 
	
	@Override
	public IScher<SysSkill> acqDao() {
		return sysSkillDao;
	}
}
