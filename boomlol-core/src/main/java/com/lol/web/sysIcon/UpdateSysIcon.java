package com.lol.web.sysIcon;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysIcon.domain.SysIcon;
import com.lol.biz.sysIcon.dao.SysIconDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updateSysIcon.do")
public class UpdateSysIcon extends UpdateCtrl<SysIcon>{
	public UpdateSysIcon(){
		
	}
	@Resource
	private SysIconDao sysIconDao; 
	
	@Override
	public ICrud<SysIcon> acqDao() {
		return sysIconDao;
	}
}
