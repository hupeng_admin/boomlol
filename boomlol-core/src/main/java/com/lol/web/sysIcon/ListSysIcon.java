package com.lol.web.sysIcon;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.lol.biz.sysIcon.dao.SysIconDao;
import com.lol.biz.sysIcon.domain.SysIcon;
@Controller
@RequestMapping("/listSysIcon")
public class ListSysIcon extends SchCtrl<SysIcon,SchParam>{
	
	public ListSysIcon(){
		this.needFormatDate = true;
	}
	@Resource
	private SysIconDao sysIconDao; 
	
	@Override
	public IScher<SysIcon> acqDao() {
		return sysIconDao;
	}
}
