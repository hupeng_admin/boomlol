package com.lol.web.sysIcon;

import com.lol.biz.sysIcon.domain.SysIcon;
import com.lol.biz.sysIcon.dao.SysIconDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addSysIcon")
public class AddSysIcon extends AddCtrl<SysIcon>{
	public AddSysIcon(){
		
	}

	@Resource
	private SysIconDao sysIconDao; 
	
	@Override
	public ICrud<SysIcon> acqDao() {
		return sysIconDao;
	}
}
