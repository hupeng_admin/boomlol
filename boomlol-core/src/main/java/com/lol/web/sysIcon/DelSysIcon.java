package com.lol.web.sysIcon;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysIcon.domain.SysIcon;
import com.lol.biz.sysIcon.dao.SysIconDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delSysIcon.do")
public class DelSysIcon extends DelCtrl<SysIcon>{
	public DelSysIcon(){
		
	}
	@Resource
	private SysIconDao sysIconDao; 
	
	@Override
	public ICrud<SysIcon> acqDao() {
		return sysIconDao;
	}
}
