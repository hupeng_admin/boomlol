package com.lol.web.sysChampion;


import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysChampion.domain.SysChampion;
import com.lol.biz.sysChampion.dao.SysChampionDao;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.UpdateCtrl;
@Controller
@RequestMapping("/updateSysChampion.do")
public class UpdateSysChampion extends UpdateCtrl<SysChampion>{
	public UpdateSysChampion(){
		
	}
	@Resource
	private SysChampionDao sysChampionDao; 
	
	@Override
	public ICrud<SysChampion> acqDao() {
		return sysChampionDao;
	}
}
