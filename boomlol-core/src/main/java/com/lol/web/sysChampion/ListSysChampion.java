package com.lol.web.sysChampion;

import com.lol.biz.sysChampion.domain.SysChampion;
import com.lol.biz.sysChampion.dao.SysChampionDao;

import com.fastwebx.common.dao.inf.IScher;
import com.fastwebx.db.condition.schObject.SchParam;
import com.fastwebx.web.SchCtrl;
import com.fastwebx.web.result.SchResult;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller
@RequestMapping("/listSysChampion")
public class ListSysChampion extends SchCtrl<SysChampion,SchParam>{
	
	public ListSysChampion(){
		this.needFormatDate = true;
	}
	@Resource
	private SysChampionDao sysChampionDao; 
	
	@Override
	public IScher<SysChampion> acqDao() {
		return sysChampionDao;
	}
}
