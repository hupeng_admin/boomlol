package com.lol.web.sysChampion;

import com.lol.biz.sysChampion.domain.SysChampion;
import com.lol.biz.sysChampion.dao.SysChampionDao;
import javax.annotation.Resource;

import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.AddCtrl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/addSysChampion")
public class AddSysChampion extends AddCtrl<SysChampion>{
	public AddSysChampion(){
		
	}

	@Resource
	private SysChampionDao sysChampionDao; 
	
	@Override
	public ICrud<SysChampion> acqDao() {
		return sysChampionDao;
	}
}
