package com.lol.web.sysChampion;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.lol.biz.sysChampion.domain.SysChampion;
import com.lol.biz.sysChampion.dao.SysChampionDao;
import javax.annotation.Resource;
import com.fastwebx.common.dao.ICrud;
import com.fastwebx.web.DelCtrl;


@Controller
@RequestMapping("/delSysChampion.do")
public class DelSysChampion extends DelCtrl<SysChampion>{
	public DelSysChampion(){
		
	}
	@Resource
	private SysChampionDao sysChampionDao; 
	
	@Override
	public ICrud<SysChampion> acqDao() {
		return sysChampionDao;
	}
}
