package com.lol.biz.playerMostUseChampion.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class PlayerMostUseChampion implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setChampionId(Long championId){
		this.championId=championId;
	}

	public Long getChampionId(){
		return this.championId;
	}
	public void setPlayerId(Long playerId){
		this.playerId=playerId;
	}

	public Long getPlayerId(){
		return this.playerId;
	}

	private Long id;

	private Long championId;

	private Long playerId;

	public  Object acqPk(){
		return id;
	}
}