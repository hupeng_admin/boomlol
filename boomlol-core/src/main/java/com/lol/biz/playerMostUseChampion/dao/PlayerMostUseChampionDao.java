package com.lol.biz.playerMostUseChampion.dao;
import org.springframework.stereotype.Repository;

import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.playerMostUseChampion.domain.PlayerMostUseChampion;

@Repository
public class PlayerMostUseChampionDao extends BaseDao<PlayerMostUseChampion>{
	
}
