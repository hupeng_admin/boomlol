package com.lol.biz.sysSkill.dao;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;
import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.sysSkill.domain.SysSkill;

@Repository
public class SysSkillDao extends BaseDao<SysSkill>{
	
}
