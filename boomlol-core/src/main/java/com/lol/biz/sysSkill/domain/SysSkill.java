package com.lol.biz.sysSkill.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class SysSkill implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Integer id){
		this.id=id;
	}

	public Integer getId(){
		return this.id;
	}
	public void setSkillName(String skillName){
		this.skillName=skillName;
	}

	public String getSkillName(){
		return this.skillName;
	}
	public void setSkillIco(Integer skillIco){
		this.skillIco=skillIco;
	}

	public Integer getSkillIco(){
		return this.skillIco;
	}
	public void setSkillPath(Long skillPath){
		this.skillPath=skillPath;
	}

	public Long getSkillPath(){
		return this.skillPath;
	}

	private Integer id;

	private String skillName;

	private Integer skillIco;

	private Long skillPath;

	public  Object acqPk(){
		return id;
	}
}