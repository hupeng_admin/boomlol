package com.lol.biz.sysChampion.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class SysChampion implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setChampionName(String championName){
		this.championName=championName;
	}

	public String getChampionName(){
		return this.championName;
	}
	public void setCname(String cname){
		this.cname=cname;
	}

	public String getCname(){
		return this.cname;
	}
	public void setEname(String ename){
		this.ename=ename;
	}

	public String getEname(){
		return this.ename;
	}
	public void setIco(String ico){
		this.ico=ico;
	}

	public String getIco(){
		return this.ico;
	}
	public void setCoreEqp(Integer coreEqp){
		this.coreEqp=coreEqp;
	}

	public Integer getCoreEqp(){
		return this.coreEqp;
	}
	public void setNote(String note){
		this.note=note;
	}

	public String getNote(){
		return this.note;
	}
	public void setPositionId(Integer positionId){
		this.positionId=positionId;
	}

	public Integer getPositionId(){
		return this.positionId;
	}
	public void setBanPoint(Double banPoint){
		this.banPoint=banPoint;
	}

	public Double getBanPoint(){
		return this.banPoint;
	}
	public void setPickPoint(Double pickPoint){
		this.pickPoint=pickPoint;
	}

	public Double getPickPoint(){
		return this.pickPoint;
	}
	public void setWinPoint(Double winPoint){
		this.winPoint=winPoint;
	}

	public Double getWinPoint(){
		return this.winPoint;
	}
	public void setPickNum(Double pickNum){
		this.pickNum=pickNum;
	}

	public Double getPickNum(){
		return this.pickNum;
	}

	private Long id;

	private String championName;

	private String cname;

	private String ename;

	private String ico;

	private Integer coreEqp;

	private String note;

	private Integer positionId;

	private Double banPoint;

	private Double pickPoint;

	private Double winPoint;

	private Double pickNum;

	public  Object acqPk(){
		return id;
	}
}