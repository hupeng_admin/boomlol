package com.lol.biz.sysChampion.dao;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;
import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.sysChampion.domain.SysChampion;

@Repository
public class SysChampionDao extends BaseDao<SysChampion>{
	
}
