package com.lol.biz.user.dao;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;
import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.user.domain.User;

@Repository
public class UserDao extends BaseDao<User>{
	
}
