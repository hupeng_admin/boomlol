package com.lol.biz.user.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;

import java.util.Date;

public class User implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setUname(String uname){
		this.uname=uname;
	}

	public String getUname(){
		return this.uname;
	}
	public void setPlayerId(Long playerId){
		this.playerId=playerId;
	}

	public Long getPlayerId(){
		return this.playerId;
	}
	public void setStatus(Integer status){
		this.status=status;
	}

	public Integer getStatus(){
		return this.status;
	}
	public void setEmail(String email){
		this.email=email;
	}

	public String getEmail(){
		return this.email;
	}
	public void setPhone(String phone){
		this.phone=phone;
	}

	public String getPhone(){
		return this.phone;
	}
	public void setSex(Integer sex){
		this.sex=sex;
	}

	public Integer getSex(){
		return this.sex;
	}
	public void setBirthday(Date birthday){
		this.birthday=birthday;
	}

	public Date getBirthday(){
		return this.birthday;
	}

	private Long id;

	private String uname;

	private Long playerId;

	private Integer status;

	private String email;

	private String phone;

	private Integer sex;

	private Date birthday;

	public  Object acqPk(){
		return id;
	}
}