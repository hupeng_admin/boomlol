package com.lol.biz.playerRankList.dao;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;
import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.playerRankList.domain.PlayerRankList;

@Repository
public class PlayerRankListDao extends BaseDao<PlayerRankList>{
	
}
