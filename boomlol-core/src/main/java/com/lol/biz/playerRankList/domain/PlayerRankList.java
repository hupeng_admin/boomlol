package com.lol.biz.playerRankList.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class PlayerRankList implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setPlayerId(Long playerId){
		this.playerId=playerId;
	}

	public Long getPlayerId(){
		return this.playerId;
	}
	public void setMostUseChampionId(Long mostUseChampionId){
		this.mostUseChampionId=mostUseChampionId;
	}

	public Long getMostUseChampionId(){
		return this.mostUseChampionId;
	}

	private Long id;

	private Long playerId;

	private Long mostUseChampionId;

	public  Object acqPk(){
		return id;
	}
}