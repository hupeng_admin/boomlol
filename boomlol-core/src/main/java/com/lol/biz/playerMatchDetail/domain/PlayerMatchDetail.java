package com.lol.biz.playerMatchDetail.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class PlayerMatchDetail implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setMatchId(Long matchId){
		this.matchId=matchId;
	}

	public Long getMatchId(){
		return this.matchId;
	}
	public void setPlayerId(Long playerId){
		this.playerId=playerId;
	}

	public Long getPlayerId(){
		return this.playerId;
	}
	public void setChampionId(Long championId){
		this.championId=championId;
	}

	public Long getChampionId(){
		return this.championId;
	}
	public void setNumKill(Integer numKill){
		this.numKill=numKill;
	}

	public Integer getNumKill(){
		return this.numKill;
	}
	public void setNumAssists(Integer numAssists){
		this.numAssists=numAssists;
	}

	public Integer getNumAssists(){
		return this.numAssists;
	}
	public void setNumDeath(Integer numDeath){
		this.numDeath=numDeath;
	}

	public Integer getNumDeath(){
		return this.numDeath;
	}
	public void setGoldEarned(Integer goldEarned){
		this.goldEarned=goldEarned;
	}

	public Integer getGoldEarned(){
		return this.goldEarned;
	}
	public void setTotalDamage(Long totalDamage){
		this.totalDamage=totalDamage;
	}

	public Long getTotalDamage(){
		return this.totalDamage;
	}
	public void setPhysicalDamage(Long physicalDamage){
		this.physicalDamage=physicalDamage;
	}

	public Long getPhysicalDamage(){
		return this.physicalDamage;
	}
	public void setMagicDamage(Long magicDamage){
		this.magicDamage=magicDamage;
	}

	public Long getMagicDamage(){
		return this.magicDamage;
	}
	public void setLargestMultiKill(Integer largestMultiKill){
		this.largestMultiKill=largestMultiKill;
	}

	public Integer getLargestMultiKill(){
		return this.largestMultiKill;
	}
	public void setLargestKillingSpree(Integer largestKillingSpree){
		this.largestKillingSpree=largestKillingSpree;
	}

	public Integer getLargestKillingSpree(){
		return this.largestKillingSpree;
	}
	public void setExp(Long exp){
		this.exp=exp;
	}

	public Long getExp(){
		return this.exp;
	}
	public void setDemagePoint(Long demagePoint){
		this.demagePoint=demagePoint;
	}

	public Long getDemagePoint(){
		return this.demagePoint;
	}
	public void setMinionsKilled(Integer minionsKilled){
		this.minionsKilled=minionsKilled;
	}

	public Integer getMinionsKilled(){
		return this.minionsKilled;
	}
	public void setMinionsKilledPerMin(Double minionsKilledPerMin){
		this.minionsKilledPerMin=minionsKilledPerMin;
	}

	public Double getMinionsKilledPerMin(){
		return this.minionsKilledPerMin;
	}
	public void setGoldEarnedPerMin(Double goldEarnedPerMin){
		this.goldEarnedPerMin=goldEarnedPerMin;
	}

	public Double getGoldEarnedPerMin(){
		return this.goldEarnedPerMin;
	}
	public void setSkill1(Integer skill1){
		this.skill1=skill1;
	}

	public Integer getSkill1(){
		return this.skill1;
	}
	public void setSkill2(Integer skill2){
		this.skill2=skill2;
	}

	public Integer getSkill2(){
		return this.skill2;
	}

	private Long id;

	private Long matchId;

	private Long playerId;

	private Long championId;

	private Integer numKill;

	private Integer numAssists;

	private Integer numDeath;

	private Integer goldEarned;

	private Long totalDamage;

	private Long physicalDamage;

	private Long magicDamage;

	private Integer largestMultiKill;

	private Integer largestKillingSpree;

	private Long exp;

	private Long demagePoint;

	private Integer minionsKilled;

	private Double minionsKilledPerMin;

	private Double goldEarnedPerMin;

	private Integer skill1;

	private Integer skill2;

	public  Object acqPk(){
		return id;
	}
}