package com.lol.biz.playerMatchDetail.dao;
import org.springframework.stereotype.Repository;

import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.playerMatchDetail.domain.PlayerMatchDetail;

@Repository
public class PlayerMatchDetailDao extends BaseDao<PlayerMatchDetail>{
	
}
