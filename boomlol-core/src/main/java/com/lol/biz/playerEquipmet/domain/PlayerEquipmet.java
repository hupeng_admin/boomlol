package com.lol.biz.playerEquipmet.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class PlayerEquipmet implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setMatchDetailId(Long matchDetailId){
		this.matchDetailId=matchDetailId;
	}

	public Long getMatchDetailId(){
		return this.matchDetailId;
	}
	public void setType(Integer type){
		this.type=type;
	}

	public Integer getType(){
		return this.type;
	}
	public void setEquipmentId(Long equipmentId){
		this.equipmentId=equipmentId;
	}

	public Long getEquipmentId(){
		return this.equipmentId;
	}

	private Long matchDetailId;

	private Integer type;

	private Long equipmentId;

	public  Object acqPk(){
		return equipmentId;
	}
}