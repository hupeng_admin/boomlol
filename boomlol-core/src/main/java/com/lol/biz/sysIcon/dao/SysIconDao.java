package com.lol.biz.sysIcon.dao;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;
import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.sysIcon.domain.SysIcon;

@Repository
public class SysIconDao extends BaseDao<SysIcon>{
	
}
