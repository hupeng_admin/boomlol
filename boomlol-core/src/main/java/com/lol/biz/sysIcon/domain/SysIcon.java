package com.lol.biz.sysIcon.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class SysIcon implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setPath(String path){
		this.path=path;
	}

	public String getPath(){
		return this.path;
	}

	private Long id;

	private String path;

	public  Object acqPk(){
		return id;
	}
}