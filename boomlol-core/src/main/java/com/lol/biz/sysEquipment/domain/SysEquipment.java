package com.lol.biz.sysEquipment.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class SysEquipment implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setEqpName(String eqpName){
		this.eqpName=eqpName;
	}

	public String getEqpName(){
		return this.eqpName;
	}
	public void setEqpIco(String eqpIco){
		this.eqpIco=eqpIco;
	}

	public String getEqpIco(){
		return this.eqpIco;
	}

	private Long id;

	private String eqpName;

	private String eqpIco;

	public  Object acqPk(){
		return id;
	}
}