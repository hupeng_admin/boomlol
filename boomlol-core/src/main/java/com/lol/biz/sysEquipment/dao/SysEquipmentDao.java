package com.lol.biz.sysEquipment.dao;
import org.springframework.stereotype.Repository;
import javax.annotation.Resource;
import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.sysEquipment.domain.SysEquipment;

@Repository
public class SysEquipmentDao extends BaseDao<SysEquipment>{
	
}
