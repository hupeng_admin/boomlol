package com.lol.biz.player.dao;
import org.springframework.stereotype.Repository;

import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.player.domain.Player;

@Repository
public class PlayerDao extends BaseDao<Player>{
	
}
