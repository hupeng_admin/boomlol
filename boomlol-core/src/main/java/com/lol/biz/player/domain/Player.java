package com.lol.biz.player.domain;
import com.fastwebx.common.dao.inf.IPk;
import java.io.Serializable;
public class Player implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setAreaId(Integer areaId){
		this.areaId=areaId;
	}

	public Integer getAreaId(){
		return this.areaId;
	}
	public void setPlayerName(String playerName){
		this.playerName=playerName;
	}

	public String getPlayerName(){
		return this.playerName;
	}
	public void setIconId(Long iconId){
		this.iconId=iconId;
	}

	public Long getIconId(){
		return this.iconId;
	}
	public void setLevel(Integer level){
		this.level=level;
	}

	public Integer getLevel(){
		return this.level;
	}
	public void setTierId(Integer tierId){
		this.tierId=tierId;
	}

	public Integer getTierId(){
		return this.tierId;
	}
	public void setQquin(String qquin){
		this.qquin=qquin;
	}

	public String getQquin(){
		return this.qquin;
	}
	public void setQueneName(String queneName){
		this.queneName=queneName;
	}

	public String getQueneName(){
		return this.queneName;
	}
	public void setTierPoint(Integer tierPoint){
		this.tierPoint=tierPoint;
	}

	public Integer getTierPoint(){
		return this.tierPoint;
	}

	private Long id;

	private Integer areaId;

	private String playerName;

	private Long iconId;

	private Integer level;

	private Integer tierId;

	private String qquin;

	private String queneName;

	private Integer tierPoint;

	public  Object acqPk(){
		return id;
	}
}