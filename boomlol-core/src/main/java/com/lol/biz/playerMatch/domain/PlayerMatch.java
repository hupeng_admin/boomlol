package com.lol.biz.playerMatch.domain;
import java.io.Serializable;
import java.util.Date;

import com.fastwebx.common.dao.inf.IPk;
import com.fastwebx.common.util.DateUtil;

public class PlayerMatch implements IPk,Serializable{
	private static final long serialVersionUID = -871092257120622327L;
	public void setId(Long id){
		this.id=id;
	}

	public Long getId(){
		return this.id;
	}
	public void setChampionId(Integer championId){
		this.championId=championId;
	}

	public Integer getChampionId(){
		return this.championId;
	}
	public void setMatchType(Integer matchType){
		this.matchType=matchType;
	}

	public Integer getMatchType(){
		return this.matchType;
	}
	public void setWin(Integer win){
		this.win=win;
	}

	public Integer getWin(){
		return this.win;
	}
	public void setPlayerId(Long playerId){
		this.playerId=playerId;
	}

	public Long getPlayerId(){
		return this.playerId;
	}
	public void setBeginTime(Date beginTime){
		this.beginTime=beginTime;
	}

	public Date getBeginTime(){
		return this.beginTime;
	}
	public void setEndTime(Date endTime){
		this.endTime=endTime;
	}

	public Date getEndTime(){
		return this.endTime;
	}
	public void setDelayTime(Long delayTime){
		this.delayTime=delayTime;
	}

	public Long getDelayTime(){
		return this.delayTime;
	}

	public void setBeginTimeByString(String beginTime){
		this.beginTime = DateUtil.parse(beginTime, DateUtil.fullTimeFmt);
	}
	
	public void setEndTimeByString(String endTime){
		this.endTime = DateUtil.parse(endTime, DateUtil.fullTimeFmt);
	}
	
	public String getBeginTimeByString(){
		return DateUtil.format(beginTime, DateUtil.fullTimeFmt);
	}
	
	public String getEndTimeByString(){
		return DateUtil.format(endTime,DateUtil.fullTimeFmt);
	}
	
	
	private Long id;

	private Integer championId;

	private Integer matchType;

	private Integer win;

	private Long playerId;
	
	private Date beginTime;

	private Date endTime;
	
	private Long delayTime;

	public  Object acqPk(){
		return id;
	}

	
}