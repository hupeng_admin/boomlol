package com.lol.biz.playerMatch.dao;
import org.springframework.stereotype.Repository;

import com.fastwebx.db.dao.BaseDao;
import com.lol.biz.playerMatch.domain.PlayerMatch;

@Repository
public class PlayerMatchDao extends BaseDao<PlayerMatch>{
	
}
