package com.lol.biz.pPlayer.dao;

import java.util.Collection;

import com.fastwebx.db.test.TestBase;
import com.lol.biz.player.dao.PlayerDao;
import com.lol.biz.player.domain.Player;

public class PlayerDaoTest extends TestBase<PlayerDao> {

	
	/**
	 * 新增一条记录
	 */
	public void add(){
		
		Player player = new Player();
		player.setAreaId(4);
		player.setIconId(1L);
		player.setId(1L);
		player.setPlayerName("冰雪玲儿");
		player.setLevel(26);
		player.setQquin("83387856");
		player.setQueneName("Ⅲ");
		player.setTierId(4);
		player.setTierPoint(1800);

		this.getServer()
				.add(player);
	}
	
	/**
	 * 清缓存
	 */
	public void clearCache(){
		this.getServer().clearCache();
	}
	
	/**
	 * 根据条件清缓存(这个还不会)
	 */
	public void clearCaches() {
		
		this.getServer().clearCache();
	}
	
	/**
	 * 如果有就查，没有就加(没理解)
	 */
	public void getOrAdd(){
		Player player = this.getServer().findOne("id",444);
		this.getServer().getOrAdd(player);
	}
	
	/**
	 * 全部删除(----)
	 */
	public void delAll(){
		Collection<Player> collection = null;
		this.getServer().delAll(collection);
	}
	

	
	/**
	 * 查询
	 */
	public void testFind(){
		this.getServer()
			.findAll()
			.forEach(player->System.out.println(player.getPlayerName()));
		
//		PlayerDao dao = this.getServer();
//		dao.findAll().forEach(pplayer->System.out.println(pplayer.getPlayerName()));
	}
	
	public void testUpd(){
		Player player = this.getServer().findOne("id",12);
		player.setPlayerName("chaodiao");
		this.getServer().update(player);
	}
	
	
	public void testDel(){
		Player player = this.getServer().findOne("id",9);
		this.getServer().del(player);
			
	}
	
	
	
	
	
	
	
}
