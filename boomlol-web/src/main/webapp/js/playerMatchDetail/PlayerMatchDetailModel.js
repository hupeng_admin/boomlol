$import('SFModel');
var PlayerMatchDetailModel = $createClass('PlayerMatchDetailModel',function(param){
	if(!param)
		param = {};
	param.name = 'playerMatchDetail';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
