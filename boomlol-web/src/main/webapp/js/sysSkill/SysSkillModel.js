$import('SFModel');
var SysSkillModel = $createClass('SysSkillModel',function(param){
	if(!param)
		param = {};
	param.name = 'sysSkill';
	this.SFModel(param);
	this.buildAction('add');
	this.buildAction('del');
	this.buildAction('update');
	this.buildQuery('list');
},'SFModel',true);
