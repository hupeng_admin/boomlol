SWImportUtil.localCtx = "../js/";

SWImportUtil.modelMap["PlayerView"]="local:player/PlayerView.js";
SWImportUtil.modelMap["PlayerModel"]="local:player/PlayerModel.js";

SWImportUtil.modelMap["PlayerEquipmetView"]="local:playerEquipmet/PlayerEquipmetView.js";
SWImportUtil.modelMap["PlayerEquipmetModel"]="local:playerEquipmet/PlayerEquipmetModel.js";

SWImportUtil.modelMap["PlayerMatchView"]="local:playerMatch/PlayerMatchView.js";
SWImportUtil.modelMap["PlayerMatchModel"]="local:playerMatch/PlayerMatchModel.js";

SWImportUtil.modelMap["PlayerMatchDetailView"]="local:playerMatchDetail/PlayerMatchDetailView.js";
SWImportUtil.modelMap["PlayerMatchDetailModel"]="local:playerMatchDetail/PlayerMatchDetailModel.js";

SWImportUtil.modelMap["PlayerMostUseChampionView"]="local:playerMostUseChampion/PlayerMostUseChampionView.js";
SWImportUtil.modelMap["PlayerMostUseChampionModel"]="local:playerMostUseChampion/PlayerMostUseChampionModel.js";

SWImportUtil.modelMap["PlayerRankListView"]="local:playerRankList/PlayerRankListView.js";
SWImportUtil.modelMap["PlayerRankListModel"]="local:playerRankList/PlayerRankListModel.js";

SWImportUtil.modelMap["SysChampionView"]="local:sysChampion/SysChampionView.js";
SWImportUtil.modelMap["SysChampionModel"]="local:sysChampion/SysChampionModel.js";

SWImportUtil.modelMap["SysEquipmentView"]="local:sysEquipment/SysEquipmentView.js";
SWImportUtil.modelMap["SysEquipmentModel"]="local:sysEquipment/SysEquipmentModel.js";

SWImportUtil.modelMap["SysIconView"]="local:sysIcon/SysIconView.js";
SWImportUtil.modelMap["SysIconModel"]="local:sysIcon/SysIconModel.js";

SWImportUtil.modelMap["SysSkillView"]="local:sysSkill/SysSkillView.js";
SWImportUtil.modelMap["SysSkillModel"]="local:sysSkill/SysSkillModel.js";

SWImportUtil.modelMap["UserView"]="local:user/UserView.js";
SWImportUtil.modelMap["UserModel"]="local:user/UserModel.js";


