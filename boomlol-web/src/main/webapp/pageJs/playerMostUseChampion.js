$import('PlayerMostUseChampionView');
$import('PlayerMostUseChampionModel');
$import('SFPage');
$ready(function(){
	var page= new SFPage({
		menuList:menuList,
		view : new PlayerMostUseChampionView(),
		model: PlayerMostUseChampionModel.get(),
		viewMsgs:['add','del','update']
	})
});
	
